# Mycroft plymouth theme and pantavisor plugin

This repository builds a Mycroft plymouth theme as an pantavisor plugin.
To build the addon, execute `./build_mycroft_plymouth.sh`.
This script copies the Mycroft plymouth theme located at `themes/` into the plymouth/themes and build and extracts the Mycroft pantavisor addon.
