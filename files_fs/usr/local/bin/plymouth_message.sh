#!/bin/sh

CONTAINER_IO_FS=/container-io
CONTAINER_MESSAGE_FILE=$CONTAINER_IO_FS/plymouth-message
message=
msg_animation1="Booting System .  "
msg_animation2="Booting System .. "
msg_animation3="Booting System ..."

echo "waiting for plymouth messages..." > /plymouth-message
while true; do
	sleep 0.5
	if [ -f $CONTAINER_MESSAGE_FILE ]; then
		message="$(cat ${CONTAINER_MESSAGE_FILE})"
		rm ${CONTAINER_MESSAGE_FILE} -f
		plymouth display-message --text="${message}"
		echo "plymouth display-message --text=\"${message}\"" >> /plymouth-message
	fi

	if [ -z "$message" ]; then
		plymouth display-message --text="${msg_animation}"
		if [ "${msg_animation}" = "${msg_animation1}" ]; then
			msg_animation="$msg_animation2"
		elif [ "${msg_animation}" = "${msg_animation2}" ]; then
			msg_animation="$msg_animation3"
		elif [ "${msg_animation}" = "${msg_animation3}" ]; then
			msg_animation="$msg_animation1"
		else
			msg_animation="$msg_animation1"
		fi

	fi
done
echo "... done" >> /plymouth-message

